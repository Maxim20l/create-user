
// 1. Метод це функція, котра знаходиться в об'єкту і виконує якісь дії з даними, що знаходяться в самому об'єкту
// 2. Об'єкт може мати всі типи даних
// 3. Об'єкт це посилальний тип даних. Тобто коли ми створюємо зміну і кажемо що це об'єкт то кожного разу коли ми будемо 
// присваювати іншій зміній цей об'єкт то ця змінна не копіює дані того об'єкта, а просто працює як силка на оригінальний об'єкт.

///////////////////////////////////////////////////////////////////////////////////////////
// function createNewUser() {
//     newUser = {};
//     newUser.firstName = prompt('Your name?');
//     newUser.lastName = prompt('Yor surname?');
//     newUser.getLogin = function() {
//         return newUser.firstName.charAt(0).toLowerCase() + newUser.lastName.toLowerCase();
      
//     };
//     return newUser;
// }
// let createneuser = createNewUser();
// console.log(createneuser.getLogin());

/////////////////////////////////////////////////////////////////////////////////////////////////////////////


function createNewUser() {
    const newUser = {
        getLogin: function() {
            return (this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase());
    },
    setNewName: function (name) {
        Object.defineProperty(this, 'firstName', {
           value: name,
            });
     },
     setLastName: function (surname) {
        Object.defineProperty(this, 'lastName', {
            value: surname
        })
     }
    };
   
    Object.defineProperties(newUser, {
        firstName: {
             value: prompt('Your name?'), 
             writable: false,
             configurable: true 
            },
        lastName: { 
            value: prompt('Your surname?'), 
            writable: false, 
            configurable: true 
        },
      });
     
    return newUser;

}
let myUser = createNewUser();

myUser.firstName = 'Kolya';
console.log(myUser.getLogin());
console.log(myUser)
myUser.lastName ='Shevchenko';
console.log(myUser.getLogin());
console.log(myUser);
myUser.setNewName('Kolya');
console.log(myUser)
console.log(myUser.getLogin());
myUser.setLastName('Shevchenko');
console.log(myUser)
console.log(myUser.getLogin());



